package ca.bnc.wealth.agile.calendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgileCalendarApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgileCalendarApplication.class, args);
	}

}
